package com.baeldung.selenium.testng;

import com.baeldung.selenium.SeleniumExample;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

// test main package Selenium example with testng
public class SeleniumWithTestNGLiveTest {

    private SeleniumExample seleniumExample;
    private static final String EXPECTED_TITLE = "About Baeldung | Baeldung";

    @BeforeSuite
    public void setUp() {
        seleniumExample = new SeleniumExample();
    }

    @AfterSuite
    public void tearDown() {
        seleniumExample.closeWindow();
    }

    @Test
    public void whenAboutBaeldungIsLoaded_thenAboutEugenIsMentionedOnPage() {
        // given
        seleniumExample.getAboutBaeldungPage();

        // when
        String actualTitle = seleniumExample.getTitle();

        // then
        assertNotNull(actualTitle);
        assertEquals(EXPECTED_TITLE, actualTitle);
        assertTrue(seleniumExample.isAuthorInformationAvailable());
    }
}
