package com.baeldung.selenium.cookies;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

// standalone selenium test, doesn't need code in main/java package
class SeleniumCookiesJUnitLiveTest {

    private static final String WEBDRIVER_GECKO_DRIVER = "webdriver.gecko.driver";
    private static final String GECKODRIVER_WIN = "src/main/resources/geckodriver.exe";
    private static final String HOME_PAGE = "https://baeldung.com";
    private static final String COOKIE_NAME = "__qca";
    private static final String BAELDUNG_COM = ".baeldung.com";
    private static final String SLASH = "/";
    private static final String FOO = "foo";
    private static final String BAR = "bar";
    private static final String AGREE_BUTTON_IDENTIFIER = "//button[text()=\"AGREE\"]";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        System.setProperty(WEBDRIVER_GECKO_DRIVER, findFile(GECKODRIVER_WIN));
        Capabilities capabilities = DesiredCapabilities.firefox();
        driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterEach
    void teardown() {
        driver.close();
    }

    @Test
    void whenNavigate_thenCookiesExist() {
        // when
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Set<Cookie> cookies = driver.manage().getCookies();

        // then
        assertThat(cookies, is(not(empty())));
    }

    @Test
    void whenNavigate_thenLpCookieExists() {
        // when
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Cookie lpCookie = driver.manage().getCookieNamed(COOKIE_NAME);

        // then
        assertThat(lpCookie, is(not(nullValue())));
    }

    @Test
    void whenNavigate_thenLpCookieHasCorrectProps() {
        // when
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Cookie lpCookie = driver.manage().getCookieNamed(COOKIE_NAME);

        // then
        assertThat(lpCookie.getDomain(), equalTo(BAELDUNG_COM));
        assertThat(lpCookie.getPath(), equalTo(SLASH));
        assertThat(lpCookie.getExpiry(), is(not(nullValue())));
        assertThat(lpCookie.isSecure(), equalTo(false));
        assertThat(lpCookie.isHttpOnly(), equalTo(false));
    }

    @Test
    void whenAddingCookie_thenItIsPresent() {
        // given
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Cookie cookie = new Cookie(FOO, BAR);

        // when
        driver.manage().addCookie(cookie);
        Cookie driverCookie = driver.manage().getCookieNamed(FOO);

        // then
        assertThat(driverCookie.getValue(), equalTo(BAR));
    }

    @Test
    void whenDeletingCookie_thenItIsAbsent() {
        // given
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Cookie lpCookie = driver.manage().getCookieNamed(COOKIE_NAME);

        // when
        driver.manage().deleteCookie(lpCookie);
        Cookie deletedCookie = driver.manage().getCookieNamed(COOKIE_NAME);

        // then
        assertThat(deletedCookie, is(nullValue()));
    }

    @Test
    void whenOverridingCookie_thenItIsUpdated() {
        // given
        driver.navigate().to(HOME_PAGE);
        acceptCookies();
        Cookie lpCookie = driver.manage().getCookieNamed(COOKIE_NAME);
        driver.manage().deleteCookie(lpCookie);
        Cookie newLpCookie = new Cookie(COOKIE_NAME, FOO);

        // when
        driver.manage().addCookie(newLpCookie);
        Cookie overriddenCookie = driver.manage().getCookieNamed(COOKIE_NAME);

        // then
        assertThat(overriddenCookie.getValue(), equalTo(FOO));
    }

    private static String findFile(String filename) {
        String[] paths = {"", "bin/", "target/classes"}; // if you have chromedriver.mac somewhere else on the path, then put it here.
        for (String path : paths) {
            if (new File(path + filename).exists())
                return path + filename;
        }
        return "";
    }

    private void acceptCookies() {
        By acceptCookies = By.xpath(AGREE_BUTTON_IDENTIFIER);
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.elementToBeClickable(acceptCookies)).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(acceptCookies));
    }

}
