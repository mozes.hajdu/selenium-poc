package com.baeldung.selenium.pages;

import com.baeldung.selenium.config.SeleniumConfig;
import com.baeldung.selenium.util.CookieHandlerImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@FieldDefaults(level = AccessLevel.PRIVATE)
class SeleniumPageObjectLiveTest {

    SeleniumConfig config;
    BaeldungHomePage homePage;
    BaeldungAboutPage about;

    @BeforeEach
    void setUp() {
        config = new SeleniumConfig();
        homePage = new BaeldungHomePage(config, new CookieHandlerImpl(config.getDriver()));
        about = new BaeldungAboutPage(config);
    }

    @AfterEach
    void teardown() {
        config.close();
    }


    @Test
    void givenHomePage_whenNavigate_thenShouldBeInStartHere() {
        // given
        homePage.navigate();

        // when
        StartHerePage startHerePage = homePage.clickOnStartHere();

        // then
        assertThat(startHerePage.getPageTitle(), is("Start Here"));
    }

    @Test
    void givenAboutPage_whenNavigate_thenTitleMatch() {
        // when
        about.navigateTo();
        // then
        assertThat(about.getPageTitle(), is("About Baeldung"));
    }
}
