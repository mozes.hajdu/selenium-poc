package com.baeldung.selenium.clickusingjavascript;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

// standalone selenium test, doesn't need code in main/java package
class SeleniumJavaScriptClickLiveTest {

    private static final String CHROME_DRIVER_PROPERTY_NAME = "webdriver.chrome.driver";
    private static final String CHROME_DRIVER_PATH = "src/main/resources/chromedriver.exe";
    private static final String HOME_PAGE_URL = "https://baeldung.com";
    private static final String HOME_PAGE_TITLE = "Baeldung";
    private static final String NAV_MENU_ITEM_ANCHOR = "nav--menu_item_anchor";
    private static final String SEARCH = "search";
    private static final String SELENIUM = "Selenium";
    private static final String BTN_SEARCH = ".btn-search";
    private static final String POST = "post";
    private static final String EXACT_ARGUMENT = "arguments[0].click();";

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    public void setUp() {
        System.setProperty(CHROME_DRIVER_PROPERTY_NAME, new File(CHROME_DRIVER_PATH).getAbsolutePath());
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 20);
    }

    @AfterEach
    void cleanUp() {
        driver.close();
    }

    @Test
    void whenOpenHomePage_thenFindCorrectTitle() {
        // given
        driver.get(HOME_PAGE_URL);

        // when
        String title = driver.getTitle();

        // then
        Assertions.assertEquals(HOME_PAGE_TITLE, title);
    }

    @Test
    void whenSearchForSeleniumArticles_thenReturnNotEmptyResults() {
        // given
        driver.get(HOME_PAGE_URL);

        wait.until(ExpectedConditions.elementToBeClickable(By.className(NAV_MENU_ITEM_ANCHOR)));
        WebElement searchButton = driver.findElement(By.className(NAV_MENU_ITEM_ANCHOR));
        clickElement(searchButton);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(SEARCH)));
        WebElement searchInput = driver.findElement(By.id(SEARCH));
        searchInput.sendKeys(SELENIUM);

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(BTN_SEARCH)));
        WebElement seeSearchResultsButton = driver.findElement(By.cssSelector(BTN_SEARCH));
        clickElement(seeSearchResultsButton);

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(POST)));

        // when
        int seleniumPostsCount = driver.findElements(By.className(POST)).size();

        // then
        Assertions.assertTrue(seleniumPostsCount > 0);
    }

    private void clickElement(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript(EXACT_ARGUMENT, element);
    }

}
