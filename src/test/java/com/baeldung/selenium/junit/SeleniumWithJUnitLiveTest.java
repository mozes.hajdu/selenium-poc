package com.baeldung.selenium.junit;

import com.baeldung.selenium.SeleniumExample;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

// test main package SeleniumExample with junit
class SeleniumWithJUnitLiveTest {

    private static final String EXPECTED_TITLE = "About Baeldung | Baeldung";

    private SeleniumExample seleniumExample;

    @BeforeEach
    void init() {
        seleniumExample = new SeleniumExample();
    }

    @AfterEach
    void tearDown() {
        seleniumExample.closeWindow();
    }

    @Test
    void whenAboutBaeldungIsLoaded_thenAboutEugenIsMentionedOnPage() {
        // given
        seleniumExample.getAboutBaeldungPage();

        // when
        String actualTitle = seleniumExample.getTitle();

        // then
        Assertions.assertNotNull(actualTitle);
        Assertions.assertEquals(EXPECTED_TITLE, actualTitle);
        Assertions.assertTrue(seleniumExample.isAuthorInformationAvailable());
    }

}
