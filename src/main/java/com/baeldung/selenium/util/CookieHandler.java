package com.baeldung.selenium.util;

public interface CookieHandler {

    void acceptCookies();
}
