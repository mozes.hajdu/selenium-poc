package com.baeldung.selenium.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CookieHandlerImpl implements CookieHandler {
    public static final String AGREE_BUTTON_IDENTIFIER = "//button[text()=\"AGREE\"]";

    WebDriver webDriver;

    @Override
    public void acceptCookies() {
        By acceptCookies = By.xpath(AGREE_BUTTON_IDENTIFIER);
        WebDriverWait wait = new WebDriverWait(webDriver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(acceptCookies)).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(acceptCookies));
    }
}
