package com.baeldung.selenium.config;

import com.baeldung.selenium.models.Browser;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SeleniumConfig {
    private static final String BROWSER_PROPERTY_NAME = "browser";
    private static final String PROPERTY_FILE_NAME = "application.properties";

    WebDriver driver;
    WebDriverFactory webDriverFactory;

    public SeleniumConfig() {
        webDriverFactory = new WebDriverFactory();
        driver = chooseBrowser();
    }

    public void close() {
        driver.close();
    }

    public void navigateTo(String url) {
        driver.navigate()
                .to(url);
    }

    public void clickElement(WebElement element) {
        element.click();
    }

    private WebDriver chooseBrowser() {
        Properties properties = getProperties();
        String browserString = properties.get(BROWSER_PROPERTY_NAME).toString().toUpperCase();
        Browser browser = Browser.valueOf(browserString);
        return webDriverFactory.create(browser);
    }

    @SneakyThrows
    private Properties getProperties() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream(PROPERTY_FILE_NAME)) {
            properties.load(resourceStream);
        } catch (IOException e) {
            throw new FileNotFoundException(PROPERTY_FILE_NAME);
        }
        return properties;
    }
}
