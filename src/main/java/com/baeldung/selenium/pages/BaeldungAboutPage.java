package com.baeldung.selenium.pages;

import com.baeldung.selenium.config.SeleniumConfig;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BaeldungAboutPage {
    public static final String ABOUT_PAGE_URL = "http://www.baeldung.com/about/";

    SeleniumConfig config;

    public BaeldungAboutPage(SeleniumConfig config) {
        this.config = config;
        PageFactory.initElements(config.getDriver(), BaeldungAboutModel.class);
    }

    public void navigateTo() {
        config.navigateTo(ABOUT_PAGE_URL);
    }

    public String getPageTitle() {
        return BaeldungAboutModel.title.getText();
    }

    public static class BaeldungAboutModel {
        public static final String TITLE_TAG = "h1";

        @FindBy(tagName = TITLE_TAG)
        private static WebElement title;
    }
}
