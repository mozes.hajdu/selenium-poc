package com.baeldung.selenium.pages;

import com.baeldung.selenium.config.SeleniumConfig;
import com.baeldung.selenium.util.CookieHandler;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BaeldungHomePage {

    SeleniumConfig config;
    CookieHandler cookieHandler;

    public BaeldungHomePage(SeleniumConfig config, CookieHandler cookieHandler) {
        this.config = config;
        this.cookieHandler = cookieHandler;
        PageFactory.initElements(this.config.getDriver(), BaeldungHomeModel.class);
    }

    public void navigate() {
        this.config.navigateTo("http://www.baeldung.com/");
    }


    public StartHerePage clickOnStartHere() {
        cookieHandler.acceptCookies();
        config.clickElement(BaeldungHomeModel.startHere);

        StartHerePage startHerePage = new StartHerePage(config);
        PageFactory.initElements(config.getDriver(), startHerePage);

        return startHerePage;
    }

    public static class BaeldungHomeModel {
        public static final String TEXT = "Start Here";

        @FindBy(partialLinkText = TEXT)
        private static WebElement startHere;
    }
}
