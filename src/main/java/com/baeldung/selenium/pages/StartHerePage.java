package com.baeldung.selenium.pages;

import com.baeldung.selenium.config.SeleniumConfig;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StartHerePage {

    public StartHerePage(SeleniumConfig config) {
        PageFactory.initElements(config.getDriver(), StartHereModel.class);
    }

    public String getPageTitle() {
        return StartHereModel.title.getText();
    }

    public static class StartHereModel {
        public static final String TITLE_SELECTOR = ".page-title";

        @FindBy(css = TITLE_SELECTOR)
        private static WebElement title;
    }
}
