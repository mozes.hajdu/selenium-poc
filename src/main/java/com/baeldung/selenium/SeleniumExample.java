package com.baeldung.selenium;

import com.baeldung.selenium.config.SeleniumConfig;
import com.baeldung.selenium.util.CookieHandler;
import com.baeldung.selenium.util.CookieHandlerImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SeleniumExample {
    private static final String URL = "http://www.baeldung.com/";
    private static final String ABOUT_SECTION_ID = "menu-item-6138";
    private static final String ABOUT_BAELDUNG_TEXT = "About Baeldung.";
    private static final String RANDOM_PAGE_TEXT = "Hey ! I'm Eugen";

    private final SeleniumConfig config;
    private final CookieHandler cookieHandler;

    public SeleniumExample() {
        config = new SeleniumConfig();
        cookieHandler = new CookieHandlerImpl(config.getDriver());
        config.getDriver()
                .get(URL);
    }

    public void closeWindow() {
        this.config.getDriver()
                .close();
    }

    public String getTitle() {
        return this.config.getDriver()
                .getTitle();
    }

    public void getAboutBaeldungPage() {
        cookieHandler.acceptCookies();
        clickAboutLink();
        clickAboutUsLink();
    }

    public boolean isAuthorInformationAvailable() {
        return this.config.getDriver()
                .getPageSource()
                .contains(RANDOM_PAGE_TEXT);
    }

    private void clickAboutLink() {
        Actions actions = new Actions(config.getDriver());
        WebElement aboutElement = this.config.getDriver()
                .findElement(By.id(ABOUT_SECTION_ID));

        actions.moveToElement(aboutElement).perform();
    }

    private void clickAboutUsLink() {
        WebElement element = this.config.getDriver()
                .findElement(By.partialLinkText(ABOUT_BAELDUNG_TEXT));
        element.click();
    }

}
